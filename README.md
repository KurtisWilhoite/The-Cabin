# The-Cabin
1st year - 1st Semester C++ Final project (The cabin - Choose your own, text-based adventure game)

The game is a choose your own adventure text-based adventure game. This means that actions input are done via typing out the command, such as using "stand up" in order to stand.
I apologize in advance as this is my very first larger scale coding project in c++, as such, it'll likely contain some optimization issues and things of that nature. It should function just fine.
In order to play game, simply compile the "game.cpp" file, then run it.
